import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Auth } from '../auth.interface';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {

  auth: Auth;

  constructor(private authService: AuthService) { }

  ngOnInit() {
    console.log('LoginComponent - ngOnInit');

  }

  onSubmit(frm: NgForm){

    console.log(frm);

    // this.auth.username = frm.value.username;
    // this.auth.password = frm.value.password;

    // this.authService.checkLogin(this.auth)
    //   .subscribe(r => console.log(r));

    window.location.href = 'admin';

    // this.route.navigate(['admin']);
  }

  ngOnDestroy (){

    console.log('LoginComponent - ngOnDestroy');

  }

}
