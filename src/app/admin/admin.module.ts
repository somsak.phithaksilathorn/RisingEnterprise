import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProductsComponent } from './products/products.component';
import { ProductTypesComponent } from './product-types/product-types.component';
import { HeaderComponent } from './header/header.component';
import { LeftMenuComponent } from './layouts/left-menu/left-menu.component';

@NgModule({
  declarations: [
    AdminComponent,
    DashboardComponent,
    ProductsComponent,
    ProductTypesComponent,
    HeaderComponent,
    LeftMenuComponent
  ],
  imports: [
    CommonModule,
    AdminRoutingModule
  ]
})
export class AdminModule { }
