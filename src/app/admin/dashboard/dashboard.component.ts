import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit, AfterViewInit{

  constructor(
    private router:Router,
  ) { }

  ngOnInit() {
    
  }

  ngAfterViewInit(): void {
   
  }

}
