import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  constructor() { }

  ngOnInit() {

    // window.location.reload();

    this.loadScript();
  }

  public loadScript() {
    let body = <HTMLDivElement> document.body;
    let script = document.createElement('script');
    script.innerHTML = '';
    script.src = 'assets/js/admin.js';
    script.async = true;
    script.defer = true;
    body.appendChild(script);

    let script2 = document.createElement('script');
    script.src = 'assets/js/pages/index.js';
    script.async = true;
    script.defer = true;
    body.appendChild(script2);
}

// ngAfterViewInit(): void {
//   const script = document.createElement('script');
//   script.src = 'assets/js/admin.js';
//   document.body.appendChild(script);
// }


}
